```bash
cd ~
ln -s ~/Projects/dotfiles/vim/ .vim
ln -s ~/Projects/dotfiles/zshrc .zshrc
ln -s ~/Projects/dotfiles/gitconfig .gitconfig
ln -s ~/Projects/dotfiles/gitignore_global .gitignore_global
ln -s ~/Projects/dotfiles/tmux.conf .tmux.conf
ln -s ~/Projects/dotfiles/bin/tmux-session.sh ~/bin/tmux-session.sh
ln -s ~/Projects/dotfiles/bin/diff-so-fancy ~/bin/diff-so-fancy
```

```bash
defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false
```
